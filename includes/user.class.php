<?php
	session_start() ;
	class User
	{
		public $user_fname ;
		public $user_lname ;
		public $user_email ;
		protected $user_id ;
		private $user_phone ;
		//public $login ;

		public function reg_user($fname,$email,$pwd1,$pwd2)
		{
			include "dbh.inc.php";
			if (empty($fname)||empty($email)||empty($pwd1)||empty($pwd2))
			{
				$_SESSION['signupmsg'] = 'It seems like some fields were left blank, please try again' ;
				return false ;
			}
			else
			{
				//Check for valid inputs
				if(!preg_match("/^[a-zA-Z]*$/",$fname))
				{
					$_SESSION['signupmsg'] = 'The name entered seems invalid, please try again without any spaces and special characters' ;
					return false ;
				}
				else
				{
					//Check if email is valid
					if(!filter_var($email,FILTER_VALIDATE_EMAIL))
					{
						$_SESSION['signupmsg'] = 'The email looks invalid, please try with another one' ;
						return false ;
					}
					else
					{
						if ($pwd1!=$pwd2)
						{
							$_SESSION['signupmsg'] = 'The passwords you entered did not match, please try again' ;
							return false ;
						}
						else
						{
							$sql = "SELECT * FROM users WHERE u_email='$email'" ;
							$result = mysqli_query($conn,$sql);
							$resultCheck=mysqli_num_rows($result);

							if ($resultCheck>0)
							{
								header("location: ../signup.php?signup=userexisting");
								$_SESSION['signupmsg'] = 'Seems like an user already exists with that email, please try with a different one or try resetting your password' ;
								return false ;
							}
							else
							{
								//Hashing the Password
								$hashedPwd = password_hash($pwd1,PASSWORD_DEFAULT);
								
								//Insert into DB
								$sql = "INSERT INTO users (u_fname,u_email,u_pwd) VALUES ('$fname','$email','$hashedPwd') ;" ;
					
								mysqli_query($conn,$sql) ;

								return true ;
							}
						}
					}
				}
			}
		}

		public function ver_email($email)
		{
			include "dbh.inc.php";
			//Insert into DB
			$sql = "UPDATE users SET u_email_ver = true WHERE u_email = '$email' ;" ;
			mysqli_query($conn,$sql) ;
		}

		public function reg_user_phone($phone,$email)
		{
			include "dbh.inc.php";
			if (empty($phone))
			{
				$_SESSION['signupmsg'] = 'Please fill the form properly' ;
				return false ;
			}
			else
			{
				//Insert into DB
				$sql = "UPDATE users SET u_phone = '$phone' WHERE u_email = '$email' ;" ;
				mysqli_query($conn,$sql) ;
				
				return true ;
			}
		}
		
		public function login($email, $pwd)
		{
			include "dbh.inc.php";
			//Check for empty inputs
			if (empty($email)||empty($pwd))
			{
				$_SESSION['loginmsg'] = 'It seems like some fields were left blank, please try again' ;
				return false;	
			}
			else
			{
				$sql = "SELECT * FROM users WHERE u_email='$email' ; " ;
				$result = mysqli_query($conn,$sql) ;
				$resultCheck = mysqli_num_rows($result) ;
				if ($resultCheck<1)
				{
					$_SESSION['loginmsg'] = 'We did not find any accounts related to that email, please consider signing up' ;
					return false;
				}
				else
				{
					if ($row=mysqli_fetch_assoc($result))
					{
						//Dehashing the Password
						$hashedPwdCheck = password_verify($pwd, $row['u_pwd']) ;
						if ($hashedPwdCheck == false)
						{
							$_SESSION['loginmsg'] = 'The password entered did not match, please try again' ;
							return false;
						}
						elseif($hashedPwdCheck == true)
						{
							//Login the User
							$_SESSION['email'] = $row['u_email'] ;
							$_SESSION['fname'] = $row['u_fname'] ;
							return true;
						}
					}
				}
			}
		}

		public function logout()
		{
			$user_fname  = NULL ;
			$user_lname = NULL ;
			$user_email = NULL ;
			$user_id = NULL ;
			$user_phone = NULL ;
			$_SESSION['u_email'] = NULL ;
			$_SESSION['u_fname'] = NULL ;
			session_unset();
			session_destroy();
			//$login = false ;
		}

		public function check_stat()
		{
			if (isset($user_email))
			{
				return $user_email ;
			}
			else
			{
				return false ;
			}
		}

	}

?>