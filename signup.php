<?php
	session_start();
	if (isset($_SESSION['email']))
  	{
    	header("Location: home1.php") ;
  	}
?>

<!DOCTYPE html>
<html>
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-113914503-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-113914503-1');
	</script>

	<title>PulseCrates | New Account</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<script type="text/javascript" src="assets/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <script type="text/javascript" src="assets/sui/semantic.min.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/sui/semantic.min.css">
    <link rel="icon" type="image/png" href="assets/img/logo.png"/>
    <style type="text/css">
    	body
    	{
    		background: url('assets/img/bg4.jpeg');
    		background-position: center center;
    		background-repeat: no-repeat;
    		background-attachment: fixed;
    		background-size: cover;
    	}
    </style>
</head>

<body>

	<div class="navbar-fixed">
		<nav>
			<div class="nav-wrapper" style="background-color: #04e3ef; text-align: center;">
				<img src="assets/img/logo1.jpg" style="height: 100%;">
			</div>
		</nav>
	</div>

	<br><br><br>
	<div class="ui centered grid stackable" style="margin-top: 20px; padding: 15px;">
		<div class="nine wide column" style="padding: 20px; color: #ffffff;">
			<h2 class="ui inverted header">Get Started Quickly</h2>
			<p style="font-size: 18px; font-weight: lighter;">Join us and start storing your files without any boundaries. Create groups, invite your colleagues, partners or classmates and start collaborating.</p>
			<br>
			<h2 class="ui inverted header">Access anywhere from any device</h2>
			<p style="font-size: 18px; font-weight: lighter;">Our responsive and simple user interface lets you access your files from any device. Use your desktop, phone or tablet to access your files, wherever you happen to be. Make changes in one, and the update will be reflected in all other devices.</p>
		</div>

		<div class="five wide column" style="background-color: #ffffff; padding: 0; opacity: 0.9;">
			<div style="width: 100%; background-color: #04e3ef; padding: 10px;">
				<h2 class="ui header" style="text-align: center; color: #ffffff;">Sign Up</h2>
			</div>
			<form action="includes/signup0.inc.php" method="POST" style="padding: 10px;">
				<div class="input-field">
					<input type="text" name="fname" placeholder="Enter your First Name">
					<label>What do we call you?</label>
				</div>
				<div class="input-field">
					<input type="email" name="email" placeholder="Enter your E-Mail">
					<label>How can we reach you?</label>
				</div>
				<div class="input-field">
					<input type="password" name="pwd1" placeholder="Enter a Strong Password (Minimum 6 characters)">
					<label>Create a Password</label>
				</div>
				<div class="input-field">
					<input type="password" name="pwd2" placeholder="Re-enter the Password">
					<label>Let's confirm the Password</label>
				</div>
				<div style="text-align: center;">
					<button type="submit" name="submit" class="ui button" style="margin: 8px; background-color: #04e3ef; color: #ffffff;">Let's Go!</button>
					<br>
					<a href="login.php" style="color: #04e3ef;">Have an account? Sign In</a>
				</div>
			</form>
			<div>
				<!-- Status Message -->
				<?php
	                if (isset($_SESSION['signupmsg']))
	                {
	                    $temp=$_SESSION['signupmsg'];
                    	echo '
	                    	<div class="ui negative message" style="margin: 5px;">
				                <i class="close icon"></i>
				                    <div class="header" style="text-align: left;">There was a problem</div>
				                <p style="text-align: left;">'.$temp.'</p>
				            </div>
				            <script type="text/javascript">
				            	$(".message .close").on("click", function()
							    {
							    	$(this).closest(".message").transition("fade");
							    });
				            </script>
	                    ';
	                    $_SESSION['signupmsg']=NULL ;
	                }
	            ?>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		 $(document).ready(function() {Materialize.updateTextFields();});
	</script>

</body>

</html>