<?php

	session_start();
	$cid = $_POST['cid'] ;
	$file = $_FILES['file'] ;
	$fileName = $_FILES['file']['name'] ; //$fileName = $file['name'] ; 
	$fileTmpName = $_FILES['file']['tmp_name'] ;
	$fileSize = $_FILES['file']['size'] ;
	$fileError = $_FILES['file']['error'] ;
	$fileType = $_FILES['file']['type'] ;
	$email=$_SESSION['email'];
	$fautname=$_SESSION['fname'];
	$fileExt = explode('.', $fileName) ;
	$fileActualExt = strtolower(end($fileExt)) ;

	$allowed = array('doc','pdf','docx','png','txt','rar','zip','jpg','jpeg') ;

	echo $cid;

	if (in_array($fileActualExt, $allowed))
	{
		if ($fileError === 0)
		{
			if ($fileSize < 20000000)
			{
				$fileNameNew = uniqid('',true).".".$fileActualExt ;
				$fileDestination = 'uploads/'.$fileNameNew ;
				move_uploaded_file($fileTmpName, $fileDestination) ;
				
				include_once 'dbh.inc.php' ;

				$sql = "INSERT INTO files (f_id,f_author,f_author_name,f_name,f_size,c_id) VALUES ('$fileNameNew','$email','$fautname','$fileName','$fileSize','$cid') ;" ;
				mysqli_query($conn,$sql) ;
				$_SESSION['home3msg2'] = 'File has been uploaded successfully' ;
				$_SESSION['ncid']=$cid ;
				header("Location: ../home3.php") ;
			}
			else
			{
				$_SESSION['home3msg'] = 'File size is too large';
				$_SESSION['ncid']=$cid ;
				header("Location: ../home3.php") ;
			}
		}
		else
		{
			$_SESSION['home3msg'] = 'There was an unexpected error';
			$_SESSION['ncid']=$cid ;
			header("Location: ../home3.php") ;
		}
	}
	else
	{
		$_SESSION['home3msg'] = 'File type not allowed';
		$_SESSION['ncid']=$cid ;
		header("Location: ../home3.php") ;
	}

?>