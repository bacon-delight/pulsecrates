<?php
	session_start();
	if (!isset($_SESSION['email']))
  	{
    	header("Location: index.php") ;
  	}
  	if(isset($_POST['cid']))
  	{
  		$cid = $_POST['cid'] ;
  		include_once 'includes/dbh.inc.php' ;
        $sql = "SELECT c_name FROM crates WHERE c_id='$cid' ;" ;
        $result = mysqli_query($conn,$sql) ;
        $row=mysqli_fetch_row($result) ;
        $cname = $row[0] ;
  	}
  	elseif (isset($_SESSION['ncid']))
  	{
  		$cid = $_SESSION['ncid'] ;
  		include_once 'includes/dbh.inc.php' ;
        $sql = "SELECT c_name FROM crates WHERE c_id='$cid' ;" ;
        $result = mysqli_query($conn,$sql) ;
        $row=mysqli_fetch_row($result) ;
        $cname = $row[0] ;
        $_SESSION['cname']=$row[0] ;
  	}
  	else
  	{
  		header("Location: home1.php");
  	}
  	$_SESSION['ncid']=null ;
?>

<!DOCTYPE html>
<html>
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-113914503-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-113914503-1');
	</script>

	<title>PulseCrates</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<script type="text/javascript" src="assets/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <script type="text/javascript" src="assets/sui/semantic.min.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/sui/semantic.min.css">
    <link rel="icon" type="image/png" href="assets/img/logo.png"/>
</head>

<body>

	<div class="navbar-fixed">
		<nav>
			<div class="nav-wrapper" style="background-color: #04e3ef; text-align: center;">
				<img src="assets/img/logo1.jpg" style="height: 100%;">
			</div>
		</nav>
	</div>

	<div class="ui sidebar vertical inverted menu" style="padding-top: 80px;">
		<center><i class="user circle icon huge" style="color: #ffffff;"></i></center>
		<h3 class="ui header inverted" style="margin-left: 15px;">
			<?php
				if (isset($_SESSION['fname']))
				{
					$temp = $_SESSION['fname'] ;
					echo "$temp";
				}
				else
				{
					echo "User";
				}
			?>
		</h3>
		<a href="includes/logout.inc.php" class="item">Logout</a>
		<h3 class="ui header inverted" style="margin-left: 15px;">Navigation</h3>
		<a href="home1.php" class="item">Home</a>
		<a href="home2.php" class="item">Crates</a>
		<a class="active item">Crate Explorer</a>
	</div>
	
	<div class="pusher">

		<div class="" style="margin: 20px;">
			<a class="circular ui icon button primary black" id="toggle">
				<i class="settings icon"></i>
				Menu
			</a>
		</div>

		<div class="ui container">
			<?php
				if ((isset($_SESSION['home3msg']))&&($_SESSION['home3msg']=="Success!"))
				{
					echo '
                    	<div class="ui positive message">
			                <i class="close icon"></i>
			                    <div class="header" style="text-align: left;">Success!</div>
			                <p style="text-align: left;">Invitation has been sent successfully to the invited user</p>
			            </div>
			            <script type="text/javascript">
			            	$(".message .close").on("click", function()
						    {
						    	$(this).closest(".message").transition("fade");
						    });
			            </script>
                    ';
				}
				elseif(isset($_SESSION['home3msg']))
				{
					$temp = $_SESSION['home3msg'] ;
					echo '
                    	<div class="ui negative message">
			                <i class="close icon"></i>
			                    <div class="header" style="text-align: left;">Something went wrong</div>
			                <p style="text-align: left;">'.$temp.'</p>
			            </div>
			            <script type="text/javascript">
			            	$(".message .close").on("click", function()
						    {
						    	$(this).closest(".message").transition("fade");
						    });
			            </script>
                    ';
				}
				if(isset($_SESSION['home3msg2']))
				{
					$temp = $_SESSION['home3msg2'] ;
					echo '
                    	<div class="ui positive message">
			                <i class="close icon"></i>
			                    <div class="header" style="text-align: left;">Action Successful</div>
			                <p style="text-align: left;">'.$temp.'</p>
			            </div>
			            <script type="text/javascript">
			            	$(".message .close").on("click", function()
						    {
						    	$(this).closest(".message").transition("fade");
						    });
			            </script>
                    ';
				}
				$_SESSION['home3msg']=null ;
				$_SESSION['home3msg2']=null ;
			?>
		</div>

		<div class="ui container" style="padding: 10px; margin-top: 30px;">

			<h2 class="ui header" style="color: #04e3ef; text-align: center;">
				<?php
					echo $cname ;
				?>
			</h2>


						


			<?php
				$cmember = $_SESSION['email'] ;
				$sql = "SELECT * FROM members WHERE c_id='$cid' AND c_member='$cmember' ; " ;
				$result = mysqli_query($conn,$sql) ;
                $resultCheck = mysqli_num_rows($result) ;
                if ($resultCheck>0)
                {
                	echo '
            		<div class="ui mini modal" style="padding: 40px;">
						<form action="includes/fileupload.inc.php" method="POST" enctype="multipart/form-data">
			    			<p style="text-align: left;">Browse File from Local System</p>
			    			<br>
			    			<input type="hidden" name="cid" value="'.$cid.'">
							<input type="file" name="file">
							<br><br><br><br>
							<center>
								<button class="ui basic green button" type="submit" name="submit">
									<i class="caret up icon"></i>
									Upload File
								</button>
							</center>
						</form>
					</div>
                	<div class="ui centered grid stackable" style="width: 100%;">
						<div class="eight wide column" style="text-align: center;">
							<button class="ui green button" id="upload">
								Upload a File
							</button>
						</div>
						<form action="includes/invite.inc.php" class="eight wide column" method="POST" style="max-width: 100%;">
							<div class="ui action input" style="max-width: 200px;">
								<input class="field" type="email" name="member" style="height: 20px;" placeholder="Enter e-mail">
										<input type="hidden" name="cid" value="'.$cid.'">
										<input type="hidden" name="cname" value="'.$cname.'">
								<button class="ui blue button" type="submit" name="submit">
									Invite Member
								</button>
							</div>
						</form>
					</div>

                	';
                }
                else
                {
                	echo '
                	<form action="includes/joincrate.inc.php" method="POST" class="ui container centered" style="text-align: left;">
						<input type="hidden" name="cid" value="'.$cid.'">
						<input type="hidden" name="cname" value="'.$cname.'">
						<button class="ui basic button green" type="submit" name="submit">
							Join this Crate
						</button>
					</form>
                	';
                }
			?>

			<br><br>

			<?php
				include_once 'includes/dbh.inc.php' ;
				$sql = "SELECT * FROM files WHERE c_id='$cid' ORDER BY f_id DESC;" ;
                $result = mysqli_query($conn,$sql) ;
                $resultCheck = mysqli_num_rows($result) ;
                if ($resultCheck>0)
                {
                    while ($row=mysqli_fetch_row($result))
                    {
                    	echo '
							<a class="fluid ui black basic button" href="includes/uploads/'.$row[0].'">
								<div class="ui grid">
									<div class="six wide column" style="text-align: left;">'.$row[3].'</div>
									<div class="six wide column" style="text-align: right;">'.$row[2].'</div>
									<div class="two wide column" style="text-align: right;">Size:'.$row[4].'</div>
								</div>
							</a>
							<br>
						';
                    }
                }
                else
                {
                	echo '<h4 class="ui header">This crate is empty, you can upload files if you are a member of this crate</h4>' ;
                }

						
			?>

		</div>

	</div>
	<script type="text/javascript">
		$('#toggle').click(function(){
			$('.ui.sidebar').sidebar('toggle');
		});
		$('#upload').click(function(){
			$('.ui.mini.modal')
			  .modal('show')
			;
		});
	</script>
</body>

</html>