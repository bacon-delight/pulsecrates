<?php
	session_start();
	if (isset($_POST['submit']))
	{
		include_once 'user.class.php';

		if ($_POST['code']!=$_SESSION['code'])
		{
			header("Location: ../signup1.php") ;
			$_SESSION['signupmsg'] = 'The code entered did not match, please try again' ;
			exit();
		}

		$user = new User();
		$signup=$user->ver_email($_SESSION["email"]);
		header("Location: ../signup2.php") ;
	}
?>