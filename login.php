<?php
	session_start();
	if (isset($_SESSION['email']))
  	{
    	header("Location: home1.php") ;
  	}
?>

<!DOCTYPE html>
<html>
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-113914503-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-113914503-1');
	</script>

	<title>PulseCrates Login</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<script type="text/javascript" src="assets/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <script type="text/javascript" src="assets/sui/semantic.min.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/sui/semantic.min.css">
    <link rel="icon" type="image/png" href="assets/img/logo.png"/>
    <style type="text/css">
    	body
    	{
    		background: url('assets/img/bg1.jpeg');
    		background-position: center center;
    		background-repeat: no-repeat;
    		background-attachment: fixed;
    		background-size: cover;
    	}
    </style>
</head>

<body>

	<div class="navbar-fixed">
		<nav>
			<div class="nav-wrapper" style="background-color: #04e3ef; text-align: center;">
				<img src="assets/img/logo1.jpg" style="height: 100%;">
			</div>
		</nav>
	</div>

	<br><br><br>
	<div class="ui centered grid stackable" style="margin-top: 30px; padding: 15px;">
		<div class="nine wide column" style="padding: 20px; color: #ffffff;">
			<center>
				<p style="font-size: 20px;">Don't have an account yet?</p>
				<h1 style="font-size: 50px;">Join Us Today!</h1>
				<br>
				<button class="ui primary button large" style="background-color: #04e3ef" onclick="window.location.href='signup.php'">Sign Up</button>
				<br><br>
			</center>
		</div>

		<div class="five wide column" style="background-color: #ffffff; padding: 0; opacity: 0.9;">
			<div style="width: 100%; background-color: #04e3ef; padding: 10px;">
				<h2 class="ui header" style="text-align: center; color: #ffffff;">Sign In</h2>
			</div>
			<form action="includes/login.inc.php" method="POST" style="padding: 10px;">
				<div class="input-field">
					<input type="email" name="email" placeholder="Enter your Username">
					<label for="email">Username</label>
				</div>
				<div class="input-field">
					<input type="password" name="pwd" placeholder="Enter your Password">
					<label for="pwd">Password</label>
				</div>
				<div style="text-align: center;">
					<button type="submit" name="submit" class="ui button" style="margin: 8px; background-color: #04e3ef; color: #ffffff;">Let's Go!</button>
					<br>
					<a href="#" style="color: #04e3ef;">Forgot Password?</a>
				</div>
			</form>
			<div>
				<!-- Status Message -->
				<?php
	                if (isset($_SESSION['loginmsg']))
	                {
	                    $temp=$_SESSION['loginmsg'];
	                    if ($temp=='Success!')
	                    {
	                    	echo '
		                    	<div class="ui positive message">
					                <i class="close icon"></i>
					                    <div class="header" style="text-align: left;">Success!</div>
					                <p style="text-align: left;">Your account has been created successfully. Please sign in to your account to use our services.</p>
					            </div>
					            <script type="text/javascript">
					            	$(".message .close").on("click", function()
								    {
								    	$(this).closest(".message").transition("fade");
								    });
					            </script>
		                    ';
	                    }
	                    else
	                    {
	                    	echo '
		                    	<div class="ui negative message">
					                <i class="close icon"></i>
					                    <div class="header" style="text-align: left;">Error!</div>
					                <p style="text-align: left;">'.$temp.'</p>
					            </div>
					            <script type="text/javascript">
					            	$(".message .close").on("click", function()
								    {
								    	$(this).closest(".message").transition("fade");
								    });
					            </script>
		                    ';
	                    }
	                    $_SESSION['loginmsg']=NULL ;
	                }
	            ?>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		 $(document).ready(function() {Materialize.updateTextFields();});
	</script>

</body>

</html>