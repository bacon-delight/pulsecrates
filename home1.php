<?php
	session_start();
	if (!isset($_SESSION['email']))
  	{
    	header("Location: index.php") ;
  	}
?>

<!DOCTYPE html>
<html>
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-113914503-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-113914503-1');
	</script>

	<title>PulseCrates</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<script type="text/javascript" src="assets/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <script type="text/javascript" src="assets/sui/semantic.min.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/sui/semantic.min.css">
    <link rel="icon" type="image/png" href="assets/img/logo.png"/>
</head>

<body>

	<div class="navbar-fixed">
		<nav>
			<div class="nav-wrapper" style="background-color: #04e3ef; text-align: center;">
				<img src="assets/img/logo1.jpg" style="height: 100%;">
			</div>
		</nav>
	</div>

	<div class="ui sidebar vertical inverted menu" style="padding-top: 80px;">
		<center><i class="user circle icon huge" style="color: #ffffff;"></i></center>
		<h3 class="ui header inverted" style="margin-left: 15px;">
			<?php
				if (isset($_SESSION['fname']))
				{
					$temp = $_SESSION['fname'] ;
					echo "$temp";
				}
				else
				{
					echo "User";
				}
			?>
		</h3>
		<a href="includes/logout.inc.php" class="item">Logout</a>
		<h3 class="ui header inverted" style="margin-left: 15px;">Navigation</h3>
		<a class="active item">Home</a>
		<a href="home2.php" class="item">Crates</a>
	</div>
	<div class="pusher">

		<div class="" style="margin: 20px;">
			<a class="circular ui icon button primary black" id="toggle">
				<i class="settings icon"></i>
				Menu
			</a>
		</div>

		<div class="ui container">
			<?php
				if (isset($_SESSION['home1msg']))
				{
					$temp = $_SESSION['home1msg'] ;
					echo '
                    	<div class="ui positive message">
			                <i class="close icon"></i>
			                    <div class="header" style="text-align: left;">Action Success!</div>
			                <p style="text-align: left;">'.$temp.'</p>
			            </div>
			            <script type="text/javascript">
			            	$(".message .close").on("click", function()
						    {
						    	$(this).closest(".message").transition("fade");
						    });
			            </script>
                    ';
                    $_SESSION['home1msg']=null ;
				}
			?>
		</div>

		<div class="ui container" style="padding: 10px; margin-top: 30px;">
			<h2 class="ui header" style="color: #04e3ef;">Pending Invitations</h2>
			<div class="ui cards">
				<?php
					$email = $_SESSION['email'] ;
					include_once 'includes/dbh.inc.php' ;
					$sql = "SELECT * FROM invites WHERE i_to='$email' ; " ;
					$result = mysqli_query($conn,$sql) ;
					$resultCheck=mysqli_num_rows($result);
					if ($resultCheck>0)
					{
						while ($row=mysqli_fetch_row($result))
	                    {
	                    	echo '
	                    		<div class="card message">
									<div class="content">
										<img class="right floated mini ui image" src="assets/img/defuser.png">
										<div class="header">
											'.$row[4].'
										</div>
										<div class="meta">
											'.$row[3].'
										</div>
										<div class="description">
											'.$row[3].' invited you to join the crate
										</div>
									</div>
									<div class="extra content">
										<form class="ui two buttons" action="includes/processinv.inc.php" method="POST">
											<input type="hidden" name="invid" value="'.$row[0].'">
											<input type="hidden" name="cname" value="'.$row[4].'">
											<input type="hidden" name="cid" value="'.$row[5].'">
											<button class="ui basic green button acc" name="accept">Approve</button>
											<button class="ui basic red button dec" name="decline">Decline</button>
										</form>
									</div>
								</div>
	                    	';
	                    }
					}
					else
					{
						echo '
								<div class="card">
									<div class="content">
										<div class="header">No Pending Invitations</div>
										<div class="description">
											Seems like you are all caught up!
										</div>
									</div>
								</div>
	                    	';
					}
				?>
			</div>
			<script type="text/javascript">
				$(".message .acc").on("click", function()
			    {
			    	$(this).closest(".message").transition("swing right");
			    });
            	$(".message .dec").on("click", function()
			    {
			    	$(this).closest(".message").transition("fly up");
			    });
            </script>
		</div>

		<div class="ui container" style="padding: 10px; margin-top: 30px;">
			<h2 class="ui header" style="color: #04e3ef;">Public Crates</h2>
			<div class="four stackable ui cards">
				<?php
					$user = $_SESSION['email'] ;
					include_once 'includes/dbh.inc.php' ;
	                $sql = "SELECT * FROM crates WHERE c_public=true ;" ;
	                $result = mysqli_query($conn,$sql) ;
	                $resultCheck = mysqli_num_rows($result) ;
	                if ($resultCheck>0)
	                {
	                    while ($row=mysqli_fetch_row($result))
	                    {
	                    	echo '
								<form class="card" action="home3.php" method="POST">
									<div class="content">
										<div class="header">'.$row[1].'</div>
										<div class="description">
											Created by '.$row[3].'
										</div>
										<input type="hidden" name="cid" value="'.$row[0].'">
									</div>
									<button type="submit" name="submit" class="ui bottom attached button">
										<i class="hand pointer icon"></i>
										Check
									</button>
								</form>
	                    	';
	                    }
	                }
	                else
	                {
	                	echo '
								<div class="card">
									<div class="content">
										<div class="header">No Crates Available</div>
										<div class="description">
											Fortunately, you can create one!
										</div>
									</div>
								</div>
	                    	';
	                }
				?>
			</div>

		</div>

	</div>

	<script type="text/javascript">
		$('#toggle').click(function(){
			$('.ui.sidebar').sidebar('toggle');
		});
		$('#create').click(function(){
			$('.ui.basic.modal').modal('show');
		});
	</script>
</body>

</html>