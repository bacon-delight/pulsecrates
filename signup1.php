<?php
	session_start();
?>

<!DOCTYPE html>
<html>
<head>
	<title>PulseCrates | New Account</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<script src="http://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E=" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <script type="text/javascript" src="assets/sui/semantic.min.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/sui/semantic.min.css">
    <link rel="icon" type="image/png" href="assets/img/logo.png"/>
    <style type="text/css">
    	body
    	{
    		background: url('assets/img/loader.gif');
    		background-color: #2F2E34;
    		background-position: center top;
    		background-size: 400px;
    		background-repeat: no-repeat;
    	}
    </style>
</head>

<body>

	<div class="navbar-fixed">
		<nav>
			<div class="nav-wrapper" style="background-color: #04e3ef; text-align: center;">
				<img src="assets/img/logo1.jpg" style="height: 100%;">
			</div>
		</nav>
	</div>

	<div class="ui container centered">
		<h1 class="ui inverted header" style="text-align: center; margin-top: 180px;">We've sent a verification code to your email address</h1>
		<h4 class="ui inverted header" style="text-align: center; font-weight: lighter;">Please consider checking your spam folder as well</h4>
	</div>
	<form action="includes/signup1.inc.php" method="POST" style="text-align: center; margin-top: 40px;">
		<br>
		<div class="ui input action">
			<input type="text" name="code" style="height: 20px; width: 120px;" placeholder="Code">
			<button name="submit" type="submit" class="ui primary button" style="background-color: #04e3ef">Verify</button>
		</div>
		<br><br>
		<a href="includes/emailver.inc.php" style="color: #ffffff;">Resend code</a>
	</form>

	<div class="ui centered container" style="width: 350px;">
		<?php
            if (isset($_SESSION['signupmsg']))
            {
                $temp=$_SESSION['signupmsg'];
            	echo '<br>
                	<div class="ui negative message" style="margin: 5px;">
		                <i class="close icon"></i>
		                    <div class="header" style="text-align: left;">There was a problem</div>
		                <p style="text-align: left;">'.$temp.'</p>
		            </div>
		            <script type="text/javascript">
		            	$(".message .close").on("click", function()
					    {
					    	$(this).closest(".message").transition("fade");
					    });
		            </script>
                ';
                $_SESSION['signupmsg']=NULL ;
            }
        ?>
	</div>

</body>

</html>