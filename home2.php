<?php
	session_start();
	if (!isset($_SESSION['email']))
  	{
    	header("Location: index.php") ;
  	}
?>

<!DOCTYPE html>
<html>
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-113914503-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-113914503-1');
	</script>

	<title>PulseCrates</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<script type="text/javascript" src="assets/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <script type="text/javascript" src="assets/sui/semantic.min.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/sui/semantic.min.css">
    <link rel="icon" type="image/png" href="assets/img/logo.png"/>
</head>

<body>

	<div class="navbar-fixed">
		<nav>
			<div class="nav-wrapper" style="background-color: #04e3ef; text-align: center;">
				<img src="assets/img/logo1.jpg" style="height: 100%;">
			</div>
		</nav>
	</div>

	<div class="ui sidebar vertical inverted menu" style="padding-top: 80px;">
		<center><i class="user circle icon huge" style="color: #ffffff;"></i></center>
		<h3 class="ui header inverted" style="margin-left: 15px;">
			<?php
				if (isset($_SESSION['fname']))
				{
					$temp = $_SESSION['fname'] ;
					echo "$temp";
				}
				else
				{
					echo "User";
				}
			?>
		</h3>
		<a href="includes/logout.inc.php" class="item">Logout</a>
		<h3 class="ui header inverted" style="margin-left: 15px;">Navigation</h3>
		<a href="home1.php" class="item">Home</a>
		<a class="active item">Crates</a>
	</div>
	<div class="pusher">

		<div class="" style="margin: 20px;">
			<a class="circular ui icon button primary black" id="toggle">
				<i class="settings icon"></i>
				Menu
			</a>
		</div>

		<div class="ui container">
			<?php
				if (isset($_SESSION['home2msg']))
				{
					$temp = $_SESSION['home2msg'] ;
					if ($temp=="Success")
					{
						echo '
	                    	<div class="ui positive message">
				                <i class="close icon"></i>
				                    <div class="header" style="text-align: left;">Success!</div>
				                <p style="text-align: left;">Your crate has been created successfully, please check your crates from the menu to access it and start storing your files</p>
				            </div>
				            <script type="text/javascript">
				            	$(".message .close").on("click", function()
							    {
							    	$(this).closest(".message").transition("fade");
							    });
				            </script>
	                    ';
					}
					else
					{
						echo '
	                    	<div class="ui negative message">
				                <i class="close icon"></i>
				                    <div class="header" style="text-align: left;">Failed to Create Crate</div>
				                <p style="text-align: left;">'.$temp.'</p>
				            </div>
				            <script type="text/javascript">
				            	$(".message .close").on("click", function()
							    {
							    	$(this).closest(".message").transition("fade");
							    });
				            </script>
	                    ';
					}
                    $_SESSION['home2msg']=null ;
				}
			?>
		</div>

		<!-- Modal for creating new crates -->
		<div class="ui basic modal">
			<div class="ui icon header">
				<i class="plus icon"></i>
		    	Create new Crate
			</div>
			<form action="includes/createcrate.inc.php" method="POST">
				<div class="content">
			    	<p>Please enter a name for your new crate</p>
			    	<input type="text" name="cname">
			    	<div class="ui selection dropdown">
			    		<input type="hidden" name="cpublic">
			    		<i class="dropdown icon"></i>
			    		<div class="default text item">Visibility (Default: Private)</div>
			    		<div class="menu">
			    			<div class="item" data-value="0">Private</div>
			    			<div class="item" data-value="1">Public</div>
			    		</div>
			    	</div>
			    </div>
			    <br><br><br>
				<div class="actions">
			    	<div class="ui red basic cancel button">
						<i class="remove icon"></i>
						Cancel
					</div>
					<button class="ui green basic ok button" type="submit" id="check" name="submit">
						<i class="checkmark icon"></i>
						Create
			    	</button>
				</div>
			</form>
			<script type="text/javascript">
		    	$('.ui.dropdown').dropdown();
		    </script>
		</div>


		<div class="ui container" style="padding: 10px; margin-top: 30px;">
			
			<button class="ui bottom attached button" id="create">
				<i class="plus icon"></i>
				Create a Crate
			</button>
			<br>
			<p class="ui">Crates are where you can store files</p>
			
			<h2 class="ui header" style="color: #04e3ef;">All Your Crates</h2>
			
			<div class="four stackable ui cards">
				<?php
					$user = $_SESSION['email'] ;
					include_once 'includes/dbh.inc.php' ;
	                $sql = "SELECT * FROM members WHERE c_member='$user' ;" ;
	                $result = mysqli_query($conn,$sql) ;
	                $resultCheck = mysqli_num_rows($result) ;
	                if ($resultCheck>0)
	                {
	                    while ($row=mysqli_fetch_row($result))
	                    {
	                    	echo '
								<form class="card" action="home3.php" method="POST">
									<div class="content">
										<div class="header">'.$row[2].'</div>
										<div class="description">
											You are a member of this crate
										</div>
										<input type="hidden" name="cid" value="'.$row[1].'">
									</div>
									<button type="submit" name="submit" class="ui bottom attached button">
										<i class="hand pointer icon"></i>
										Check
									</button>
								</form>
	                    	';
	                    }
	                }
	                else
	                {
	                	echo '
								<div class="card">
									<div class="content">
										<div class="header">No Crates Available</div>
										<div class="description">
											Fortunately, you can create one!
										</div>
									</div>
								</div>
	                    	';
	                }
				?>
			</div>



			<br>
			<h2 class="ui header" style="color: #04e3ef;">Your Private Crates</h2>
			
			<div class="four stackable ui cards">
				<?php
					$user = $_SESSION['email'] ;
					include_once 'includes/dbh.inc.php' ;
	                $sql = "SELECT * FROM crates WHERE c_admin='$user' AND c_public=0 ;" ;
	                $result = mysqli_query($conn,$sql) ;
	                $resultCheck = mysqli_num_rows($result) ;
	                if ($resultCheck>0)
	                {
	                    while ($row=mysqli_fetch_row($result))
	                    {
	                    	echo '
								<form class="card" action="home3.php" method="POST">
									<div class="content">
										<div class="header">'.$row[1].'</div>
										<div class="description">
											Created by you
										</div>
										<input type="hidden" name="cid" value="'.$row[0].'">
									</div>
									<button type="submit" name="submit" class="ui bottom attached button">
										<i class="hand pointer icon"></i>
										Check
									</button>
								</form>
	                    	';
	                    }
	                }
	                else
	                {
	                	echo '
								<div class="card">
									<div class="content">
										<div class="header">No Crates Available</div>
										<div class="description">
											Fortunately, you can create one!
										</div>
									</div>
								</div>
	                    	';
	                }
				?>
			</div>


		</div>


	</div>

	<script type="text/javascript">
		$('#toggle').click(function(){
			$('.ui.sidebar').sidebar('toggle');
		});
		$('#create').click(function(){
			$('.ui.basic.modal').modal('show');
		});
	</script>
</body>

</html>